#pragma once
#ifndef __HEADER_H
#define __HEADER_H
#include <fstream>
#include <string>
#include <list>
#include <iterator>
#include <algorithm>

namespace Module1
{
	int sort_strings(std::list<std::string>* s);
}

#endif