#include "header.h"

using std::list;
using std::string;

namespace Module1
{
	int sort_strings(list<string>* s )
	{
		(*s).sort();
		return 0;
	}
}
