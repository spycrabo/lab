#include "header.h"

using Module1::sort_strings;

int main(int argc, char **argv)
{
	if (argc < 3) {
		std::cout << "Недостаточно аргументов!" << std::endl;
		return 0;
	}
	std::ifstream in(argv[1]);
	std::ofstream out(argv[2]);
	std::list<std::string> str;

	if ((!in) || (!out)) {
		std::cout << "Ошибка при открытии файлов!" << std::endl;
	}

	while (!in.eof()) {
		std::string s;
		std::getline(in, s, '\n');
		str.push_back(s);
	}
	
	sort_strings(str);

	copy(str.begin(), str.end(), std::ostream_iterator<std::string>(out, "\n"));
	in.close();
	out.close();
	return 0;
}
